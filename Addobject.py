'''
Write a function that takes two or more objects and returns a new object which combines all the input objects.

All input object properties will have only numeric values. Objects are combined together so that the values of matching keys are added together.

'''




def add_obj(objs):     # function takes list of objects and returns list of objects whose size is one less
    if len(objs)==1:   # or returns the only object if the list has one object
        return objs[0]
    p = objs[0]
    for elem in p:
        if elem in objs[1]:
            objs[0][elem]+=objs[1][elem]  # updation of the list, add second object to the first and delete second
            objs[1].pop(elem)

    objs[0].update(objs[1])
    del(objs[1])
    return add_obj(objs)

def add_obj_unknown(*args): # function takes any number of arguments (objects) and returns the combined object
    return add_obj(list(args))


