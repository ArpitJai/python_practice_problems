'''
The Vigenère cipher is a classic cipher originally developed by Italian cryptographer Giovan Battista Bellaso and published in 1553. It is named after a later French cryptographer Blaise de Vigenère, who had developed a stronger autokey cipher (a cipher that incorporates the message of the text into the key). We're going to implementing Vigenère's autokey cipher.

The cipher is easy to understand and implement, but survived three centuries of attempts to break it, earning it the nickname "le chiffre indéchiffrable" or "the indecipherable cipher."

From Wikipedia:

    The Vigenère cipher is a method of encrypting alphabetic text by using a series of different Caesar ciphers based on the letters of a keyword. It is a simple form of polyalphabetic substitution.


In a Caesar cipher, each letter of the alphabet is shifted along some number of places; for example, in a Caesar cipher of shift 3, A would become D, B would become E, Y would become B and so on. The Vigenère cipher consists of several Caesar ciphers in sequence with different shift values.

The shift is derived by applying a Caesar shift to a character with the corresponding index of the key in the alphabet.

With the basic Vigenère Cipher, we assume the key is repeated for the length of the text, character by character. Key is only used once, and then replaced by the decoded text. Every encoding and decoding is independent (still using the same key to begin with). The key index is only incremented if the current letter is in the provided alphabet.

Visual representation (suggested by OverZealous):

message: my secret code i want to secure
key:     pa ssword myse c retc od eiwant

Write a class that, when given a key and an alphabet, can be used to encode and decode from the cipher.


'''



class VigenereCipher():
    def __init__(self,key,alphabet):
        self.key =  key.upper()
        self.alphabet = alphabet
        self.len = len(key)
        self.list  = [ord(x)-65 for x in self.key]

    def encode(self,string):  # Encoding function for the cipher
        list2 = list(string)
        k=0;
        for i in range(0,len(string)):
            if string[i] in self.alphabet : #if the current letter is in alphabet ,increment key index and update key
                if string[i].istitle():
                    list2[i] = chr((ord(string[i])-65 + self.list[k%self.len])%26 +65)
                    self.list[k%self.len] = ord(string[i])-65
                else :
                    list2[i] = chr((ord(string[i])-97 + self.list[k%self.len])%26 +97)
                    self.list[k%self.len] = ord(string[i])-97
                k=k+1
            else :
                list2[i] = string[i]

        self.list  = [ord(x.upper())-65 for x in self.key] #make the key the default key once encoding is done

        return ''.join(list2)

    def decode(self,string):
        list2 = list(string)
        k=0;
        for i in range(0,len(string)):

            if string[i] in self.alphabet :
                if string[i].istitle():
                    list2[i] = chr((ord(string[i])-65 - self.list[k%self.len])%26 +65)
                    self.list[k%self.len] = ord(list2[i])-65
                else :
                    list2[i] = chr((ord(string[i])-97 - self.list[k%self.len])%26 +97)
                    self.list[k%self.len] = ord(list2[i])-97
                k=k+1
            else :
                list2[i] = string[i]

        self.list  = [ord(x.upper())-65 for x in self.key]

        return ''.join(list2)


'''
    def encode(self,string):
        if (len(string)<= self.len):
            return self.encode2(string)
        print(self.list)
        list2 = self.list
        for i in range(0,self.len):
            if string[i] in self.alphabet :
                if string[i].istitle():
                    list2[i] = chr((ord(string[i])-65 + self.list[i])%26 +65)
                else :
                    list2[i] = chr((ord(string[i])-97 + self.list[i])%26 +97)
            else :
                list2[i] = string[i]

        self.list  = [ord(x.upper())-65 for x in string[0:self.len]]

        return ''.join(list2) + self.encode(string[self.len:])


    def encode2(self,string):
        list2 = self.list
        print(self.list)
        for i in range(0,len(string)):
            if string[i] in self.alphabet:
                if string[i].istitle():
                    list2[i] = chr((ord(string[i])-65 + self.list[i])%26 +65)
                else :
                    list2[i] = chr((ord(string[i])-97 + self.list[i])%26 +97)
            else:
                list2[i] = string[i]
        self.list  = [ord(x)-65 for x in self.key]

        return ''.join(list2[0:len(string)])



    def decode(self,string):
        print(len(string))
        print(self.len)
        if (len(string)<= self.len):
            return self.decode2(string)
        print(self.list)
        list2 = self.list
        for i in range(0,self.len):
            if string[i] in self.alphabet :
                if string[i].istitle():
                    list2[i] = chr((ord(string[i])-65 - self.list[i])%26 +65)
                else :
                    list2[i] = chr((ord(string[i])-97 - self.list[i])%26 +97)
            else:
                list2[i] = string[i]

        self.list  = [ord(x.upper())-65 for x in list2]

        return ''.join(list2) + self.decode(string[self.len:])


    def decode2(self,string):
        list2 = self.list
        print(self.list)
        for i in range(0,len(string)):
            if string[i] in self.alphabet:
                if string[i].istitle():
                    print(string[i])
                    list2[i] = chr((ord(string[i])-65 - self.list[i])%26 +65)
                else :
                    print(string[i])
                    print(self.list[i])
                    list2[i] = chr((ord(string[i])-97 - self.list[i])%26 +97)
            else:
                list2[i] = string[i]

        self.list  = [ord(x)-65 for x in self.key]

        return ''.join(list2[0:len(string)])

'''





