from operator import itemgetter
from operator import itemgetter
import sys

'''
N friends gathered in order to play chess, according to the following rules. In the first game, two of the N friends will play.
In the second game, the winner of the first game will play against another friend (maybe even the same friend who lost the first game).
In the third game, the winner of the second game will play against someone else and so on.. No game will end as a draw (tie).
Given the number of games each of the N friends played, find a schedule for the games, so that the above rules are obeyed.
'''

def ChessPlay(list):  #function takes only one argument list of integers
    dict_list =[]
    sum=0
    for i in range(0,len(list)):
        dict= {'name':i,'number':list[i]}            #Convert the list of integers to list of dictionaries. Dictionaries contain the number(the integer in the list)
        dict_list.append(dict)                        # and the name(the index of that integer in the list)
        sum+=list[i]

    for i in range(0, len(dict_list)):
            if(dict_list[i]['number'] == 0):            #remove the players playing no match
                dict_list.remove(dict_list[i])

    print("total number of games are ",int(sum/2))

    dict_list = sorted(dict_list, key=itemgetter('number'),reverse=True)   #At any point of time match is played between the players who have maximum number of matches remaining
                                                                           #and minimum number of matches remaining and keep updating the players and removing the plyers
    while(len(dict_list) >1):                                              #who have zero match remaining

        print('match between ', dict_list[0]['name'], ' and ', dict_list[len(dict_list)-1]['name'])
        if(dict_list[len(dict_list)-1]['number'] != 1):
            print('winner is ',dict_list[len(dict_list)-1]['name'])
        else:
            print('winner is ',dict_list[0]['name'])
        dict_list[-1]['number']  = dict_list[-1]['number'] - 1
        dict_list[0]['number']  = dict_list[0]['number'] - 1

        i=0
        while i+1<len(dict_list) and (dict_list[i]['number']<dict_list[i+1]['number']): #sorting after updating
            temp = dict_list[i]
            dict_list[i]=dict_list[i+1]
            dict_list[i+1]=temp
            i = i+1

        while len(dict_list)>0 and dict_list[-1]['number']==0 :
            dict_list.remove(dict_list[-1])

if __name__ == '__main__':
    '''
    print("Input an integer (Number of Plyers)")
    fname = int(input())
    '''
    fname = sys.argv[1]
    print("input %s non negative integers seperated by space" %fname)
    list= [int(x) for x in input().split()]
    ChessPlay(list)