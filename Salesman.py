'''

A traveling salesman has to visit clients. He got each client's address e.g. "432 Main Long Road St. Louisville OH 43071" as a list.

The basic zipcode format usually consists of two capital letters followed by a white space and five digits. The list of clients to visit was given as a string of all addresses, each separated from the others by a comma, e.g. :

"123 Main Street St. Louisville OH 43071,432 Main Long Road St. Louisville OH 43071,786 High Street Pollocksville NY 56432".

To ease his travel he wants to group the list by zipcode.
Task

The function travel will take two parameters r (list of all clients' addresses) and zipcode and returns a string in the following format:

zipcode:street and town,street and town,.../nb,nb,...

The street numbers must be in the same order as the streets where they belong.

If a given zipcode doesn't exist in the list of clients' addresses return "zipcode:/"
'''
import re


def Address(list, code):   #function takes a list of addresses and a zip code
    newlist = list.split(",")
    find_list = []
    num_list  = []
    for address in newlist:
        result = re.match( "(^.* |^)"+code + "($| .*$)" , address ) #look if zip code is there in the address
        if result : #add address to the new list if zip code is found
            if re.match( "^.* "+code + "($| .*$)" , address ):   #remove zip code from the address before adding
                to_add = re.sub(" "+code,"",address)
            if re.match( "^.* "+code + " .*$" , address ):
                to_add = re.sub(code+ " ","",address)
            if re.match("^\d* .*",to_add):              #remove house number from the address assuming address starts from the house number
                print("sd")
                s=int(re.search("^\d* ",to_add).start())
                t=int(re.search("^\d* ",to_add).end())
                find_list.append(re.sub("^\d* ","",to_add))
                num_list.append(to_add[s:t-1])


            else :
                find_list.append(to_add)
    print(find_list)

    return code + ":" + ','.join(find_list)   +"/" +','.join(num_list)

