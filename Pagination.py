'''
4. complete the PaginationHelper class, which is a utility class helpful for querying paging information related to an array.

The class is designed to take in an array of values and an integer indicating how many items will be allowed per each page. The types of values contained within the collection/array are not relevant.

The following are some examples of how this class is used:

helper = PaginationHelper(['a','b','c','d','e','f'], 4)
helper.page_count # should == 2
helper.item_count # should == 6
helper.page_item_count(0)  # should == 4
helper.page_item_count(1) # last page - should == 2
helper.page_item_count(2) # should == -1 since the page is invalid
'''
# TODO: complete this class

class PaginationHelper:

  # The constructor takes in an array of items and a integer indicating
  # how many items fit within a single page
  def __init__(self, collection, items_per_page):
      self.ipp = items_per_page
      self.col = collection



  # returns the number of items within the entire collection
  def item_count(self):
      return len(self.col)


  # returns the number of pages
  def page_count(self):
      page_count = int((self.item_count()-1)/self.ipp) +1
      return page_count


  # returns the number of items on the current page. page_index is zero based
  # this method should return -1 for page_index values that are out of range
  def page_item_count(self,page_index):
      if page_index < self.page_count()-1:
          return self.ipp
      elif page_index == self.page_count()-1:
          return self.item_count() -( self.ipp * page_index)
      else :
          return -1



  # determines what page an item is on. Zero based indexes.
  # this method should return -1 for item_index values that are out of range
  def page_index(self,item_index):
      if item_index >= self.item_count():
          return -1
      else:
          return int(item_index/self.ipp) 
