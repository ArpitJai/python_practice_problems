import math
'''
Create a Vector object that supports addition, subtraction, dot products, and norms.
'''
class Vector():
    def __init__(self,list):
        l = len(list)
        self.list = list

    def add(self,other):
        if len(self.list) != len(other.list):
            print("vector cannot be added")

        else :
            list2 = []
            for i in range(0,len(self.list)):
                list2.append(self.list[i] + other.list[i])
            return Vector(list2)

    def subtract(self,other):
        if len(self.list) != len(other.list):
            print("vector cannot be subtracted")

        else :
            list2 = []
            for i in range(0,len(self.list)):
                list2.append(self.list[i] - other.list[i])
            return Vector(list2)

    def dotp(self,other):
        if len(self.list) != len(other.list):
            print("Dimensions do not match")

        else :
            product=0
            for i in range(0,len(self.list)):
                product += self.list[i] * other.list[i]
            return product

    def norm(self):
        return self.dotp(self)**(0.5)


